import './customcss/style.css'
import { BrowserRouter, Routes, Route } from 'react-router-dom';

//importar nuestros componentes
import ShowUsers from './components/ShowUsuarios';
import CreateUser from './components/CreateUser';
import EditUser from './components/EditUser';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path='/' element={ <ShowUsers/> } />
          <Route path='/create' element={ <CreateUser/> } />
          <Route path='/edit/:id' element={ <EditUser/> } />
        </Routes>
      </BrowserRouter>      
    </div>
  );
}

export default App;
