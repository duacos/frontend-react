import axios from "axios";
import React, {useState, useEffect} from "react";
import { useNavigate, useParams } from "react-router-dom";

const endpoint = 'http://localhost:8080/usuario/'

const EditUser = () => {
    const [nombre, setNombre] = useState('')
    const [email, setEmail] = useState('')
    const [edad, setEdad] = useState(0)
    const navigate = useNavigate()
    const {id} = useParams()
    
    const update = async (e) => {
        e.preventDefault()
        await axios.post(`${endpoint}`, { 
            id: id,
            nombre: nombre,
            email: email,
            edad: edad
        })
        navigate('/')
    }
    
    useEffect( () =>{
        const getUsertById = async () => {
            const response = await axios.get(`${endpoint}${id}`)
            setNombre(response.data.nombre)
            setEmail(response.data.email)
            setEdad(response.data.edad)
        }
        getUsertById()

    }, [id] )

    return (
        <div>
        <h3>Edit User</h3>
        <form onSubmit={update}>
            <div className='mb-3'>
                <label className='form-label'>Nombre</label>
                <input 
                    value={nombre}
                    onChange={ (e)=> setNombre(e.target.value)}
                    type='text'
                    className='form-control'
                />
            </div>
            <div className='mb-3'>
                <label className='form-label'>Email</label>
                <input 
                    value={email}
                    onChange={ (e)=> setEmail(e.target.value)}
                    type='email'
                    className='form-control'
                />
            </div>
            <div className='mb-3'>
                <label className='form-label'>Edad</label>
                <input 
                    value={edad}
                    onChange={ (e)=> setEdad(e.target.value)}
                    type='number'
                    className='form-control'
                />
            </div>
            <button type='submit' className='btn btn-primary'>Update</button>
        </form>
    </div>
    )
}

export default EditUser;

