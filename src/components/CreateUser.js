import axios from 'axios'
import React, {useState} from 'react'
import { useNavigate } from 'react-router-dom'

const endpoint = 'http://localhost:8080/usuario'

const CreateUser = () => {
    const [nombre, setNombre] = useState('')
    const [email, setEmail] = useState('')
    const [edad, setEdad] = useState(0)
    const navigate = useNavigate()

    const createUser = async (e) => {
        e.preventDefault()
        await axios.post(endpoint, { nombre, email, edad })
        navigate('/')
    }
    
  return (
    <div>
        <h3>Create User</h3>
        <form onSubmit={createUser}>
            <div className='mb-3'>
                <label className='form-label'>Nombre</label>
                <input 
                    value={nombre}
                    onChange={ (e)=> setNombre(e.target.value)}
                    type='text'
                    className='form-control'
                />
            </div>
            <div className='mb-3'>
                <label className='form-label'>Email</label>
                <input 
                    value={email}
                    onChange={ (e)=> setEmail(e.target.value)}
                    type='email'
                    className='form-control'
                />
            </div>
            <div className='mb-3'>
                <label className='form-label'>Edad</label>
                <input 
                    value={edad}
                    onChange={ (e)=> setEdad(e.target.value)}
                    type='number'
                    className='form-control'
                />
            </div>
            <button type='submit' className='btn btn-primary'>Save</button>
        </form>
    </div>
  )
}

export default CreateUser;