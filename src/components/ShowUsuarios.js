import React, {useEffect, useState} from 'react'
import axios from 'axios'
import {Link} from 'react-router-dom'

const endpoint = 'http://localhost:8080'

const ShowUsuarios = () => {
  const [ usuarios, setUsuarios ] = useState( [] )

  useEffect ( ()=> {
    getAllUsuarios()
  }, [])

  const getAllUsuarios = async () => {
    const response = await axios.get(`${endpoint}/usuario`)
    setUsuarios(response.data)
  }

  const deleteUsuario = async (id) => {
    await axios.delete(`${endpoint}/usuario/${id}`)
    getAllUsuarios()
  }

  return (
    <div>
        <div className='d-grid gap-2'>
            <Link to="/create" className='btn btn-success btn-lg mt-2 mb-2 text-white'>Create</Link>
        </div>

        <table className='table table-striped'>
            <thead className='bg-primary text-white'>
                <tr>
                    <th>Nombres</th>
                    <th>Email</th>
                    <th>Edad</th>
                    <th>Acción</th>
                </tr>
            </thead>
            <tbody>
                { usuarios.map( (usuario) => (
                    <tr key={usuario.id}>
                        <td> {usuario.nombre} </td>    
                        <td> {usuario.email} </td>    
                        <td> {usuario.edad} </td>    
                        <td>
                            <Link to={`/edit/${usuario.id}`} className='btn btn-warning'>Edit</Link>
                            <button onClick={ ()=>deleteUsuario(usuario.id) } className='btn btn-danger'>Delete</button>
                        </td>

                    </tr>
                )) }
            </tbody>
        </table>
    </div>
  )
}

export default ShowUsuarios;